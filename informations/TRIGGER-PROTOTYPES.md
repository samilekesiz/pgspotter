## Trigger prototypes based on discovery rules

Discovery rule | Severity | Trigger name | Create enabled
--- | --- | --- | ---
PG Database Discovery | Warning | [{#DBNAME}] DB Size is too large on {HOST.NAME} | Yes
PG Database Discovery | Information | [{#DBNAME}] Deadlocks occurred too frequently on {HOST.NAME} | Yes
PG Database Discovery | Information | [{#DBNAME}] Too many temp bytes on {HOST.NAME} | Yes
PG Schema Discovery | Warning | [{#DBNAME} / {#SCHNAME}] Schema Size is too large on {HOST.NAME} | Yes
