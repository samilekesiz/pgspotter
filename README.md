# pgSpotter

pgSpotter is a `Zabbix template` that allows `monitoring PostgreSQL`.

With this project, you can monitor your PostgreSQL servers easily.

## Item, Trigger, Discovery etc. list

* Item list: `informations/ITEMS.md`
* Trigger list: `informations/TRIGGERS.md`
* Discovery rules: `informations/DISCOVERIES.md`
* Item prototype list: `informations/ITEM-PROTOTYPES.md`
* Trigger prototype list: `informations/TRIGGER-PROTOTYPES.md`
* Macro list: `informations/MACROS.md`

## Prerequisites

Before you begin, ensure you have met the following requirements:
* Please read all of the manual because if you want to setup differently, you can find some tips between the lines. :)
* You have installed the `zabbix-agent` package or built with source code.
* You have a `Linux` machine.
* You have a `PostgreSQL 11/12`

## Installing pgSpotter

To install pgSpotter, follow these steps:

1. Clone this repository to `/etc/zabbix` or somewhere different.
```
cd /etc/zabbix
git clone https://gitlab.com/gunduzbilisim/pgspotter.git
```
* _*NOTE:*_ If you want to use another script path, you must change `{$SCRIPT_PATH}` variable from template's macro section.

2. Change all files owner to `zabbix` Linux user
```
chown -R zabbix: /etc/zabbix/pgspotter
```

3. Copy the `Zabbix Agent` configuration file to `/etc/zabbix/zabbix_agent.d` directory:
```
cp /etc/zabbix/pgspotter/zabbix_agentd.d/pgspotter.conf /etc/zabbix/zabbix_agentd.d/pgspotter.conf
```

3. Create a PostgreSQL user to run some SQL commands for monitoring purposes:
```
CREATE USER pgspotter IN ROLE pg_monitor;
```
* _*NOTE:*_ If you want to use another user for monitoring, you must change `{$PG_USER}` variable from template's macro section.
* _*NOTE:*_ If you want to work `Schema Discovery` and `Table Discovery` related settings, you must give `USAGE` permissions on all schemas to `pgspotter` user;

4. You need to add this line to `pg_ident.conf`:
```
monitoring zabbix pgspotter
```

5. You need to add this line to `pg_hba.conf`:
```
local all pgspotter ident map=monitoring
```

6. You can import the `pgSpotter-PostgreSQL_Template.xml` file to your `Zabbix` from `Zabbix Web Interface`. This templates uses this `rules`:
* Templates
* Template screens
* Applications
* Items
* Discovery rules
* Triggers
* Graphs
* Screens


## Contributing to pgSpotter

To contribute to pgSpotter, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name><location>`
5. Create the pull request.

Alternatively see the GitLab documentation on [creating a merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

## Contributors

Thanks to the following people who have contributed to this project:

* [@sedahalacli](https://gitlab.com/sedahalaclii)
* [@devrimgunduz](https://gitlab.com/devrimgunduz)
