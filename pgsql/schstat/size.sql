--------------------------------------------
--
-- Discovery rule: PG Schema Discovery
-- Application: {#DBNAME} - Schema Statistics
-- Item key: pgsql.schema.size
-- Item name: [{#DBNAME} / {#SCHNAME}] Schema Size
--
--------------------------------------------

SELECT SUM(pg_total_relation_size(quote_ident(schemaname) || '.' || quote_ident(tablename)))::BIGINT FROM pg_tables WHERE schemaname = :SCHNAME;
