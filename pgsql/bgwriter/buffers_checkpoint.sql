---------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_checkpoint
-- Item name: Number of buffers written during checkpoints
--
---------------------------------------------------------------

SELECT
    buffers_checkpoint
FROM
    pg_stat_bgwriter;
