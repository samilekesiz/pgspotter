------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.checkpoints_timed
-- Item name: Number of scheduled checkpoints that have been performed
--
------------------------------------------------------------------------

SELECT
    checkpoints_timed
FROM
    pg_stat_bgwriter;
