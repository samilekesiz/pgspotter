---------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_backend_fsync
-- Item name: Number of times a backend had to execute its own fsync call
--
---------------------------------------------------------------------------

SELECT
    buffers_backend_fsync
FROM
    pg_stat_bgwriter;
