---------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_backend
-- Item name: Number of buffers written directly by a backend
--
---------------------------------------------------------------

SELECT
    buffers_backend
FROM
    pg_stat_bgwriter;
