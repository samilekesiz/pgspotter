---------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_clean
-- Item name: Number of buffers written by the background writer
--
---------------------------------------------------------------

SELECT
    buffers_clean
FROM
    pg_stat_bgwriter;
