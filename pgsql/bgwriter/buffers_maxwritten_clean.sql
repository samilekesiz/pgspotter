-----------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_maxwritten_clean
-- Item name: Number of times the background writer stopped a cleaning scan
--
-----------------------------------------------------------------------------

SELECT
    maxwritten_clean
FROM
    pg_stat_bgwriter;
