-------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.checkpoints_req[
-- Item name: Number of requested checkpoints that have been performed
--
-------------------------------------------------------------------------

SELECT
    checkpoints_req
FROM
    pg_stat_bgwriter;
