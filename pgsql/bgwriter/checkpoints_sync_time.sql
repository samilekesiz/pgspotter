----------------------------------------------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.checkpoints_sync_time
-- Item name: Time that has been spent checkpoint where files are synchronized to disk
--
----------------------------------------------------------------------------------------

SELECT
    checkpoint_sync_time
FROM
    pg_stat_bgwriter;
