------------------------------------------------
--
-- Application: PG Background Writer Statistics
-- Item key: pgsql.bgwriter.buffers_alloc
-- Item name: Number of buffers allocated
--
------------------------------------------------

SELECT
    buffers_alloc
FROM
    pg_stat_bgwriter;
