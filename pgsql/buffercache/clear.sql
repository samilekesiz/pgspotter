---------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.buffercache.clear
-- Item name: [{#DBNAME}] Count of clear pages (pg_buffercache)
--
---------------------------------------------------------------------

SELECT
    count(*)
FROM
    pg_buffercache
WHERE
    NOT isdirty;
