---------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.buffercache.used
-- Item name: [{#DBNAME}] Count of used pages (pg_buffercache)
--
---------------------------------------------------------------------

SELECT
    count(*)
FROM
    pg_buffercache
WHERE
    reldatabase IS NOT NULL;
