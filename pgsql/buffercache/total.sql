---------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.buffercache.total
-- Item name: [{#DBNAME}] Count of all pages (pg_buffercache)
--
---------------------------------------------------------------------

SELECT
    count(*)
FROM
    pg_buffercache;
