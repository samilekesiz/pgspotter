---------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.buffercache.dirty
-- Item name: [{#DBNAME}] Count of dirty pages (pg_buffercache)
--
---------------------------------------------------------------------

SELECT
    count(*)
FROM
    pg_buffercache
WHERE
    isdirty;
