---------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.pg_stat_statements.avg_query_time
-- Item name: [{#DBNAME}] Average time spent in the statement (pg_stat_statements)
--
---------------------------------------------------------------------

SELECT
    round((sum(total_time) / sum(calls))::numeric, 2)
FROM
    pg_stat_statements;
