-------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.pg_stat_statements.write_io_time
-- Item name: [{#DBNAME}] Total time the statement spent writing blocks (pg_stat_statements)
--
-------------------------------------------------------------------------------------------------

SELECT
    sum(blk_write_time) / float4(100)
FROM
    pg_stat_statements;
