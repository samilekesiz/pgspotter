------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.pg_stat_statements.read_bytes
-- Item name: [{#DBNAME}] Total number of blocks read by the statement (pg_stat_statements)
--
------------------------------------------------------------------------------------------------

SELECT
    sum(shared_blks_read + local_blks_read + temp_blks_read) * 8 * 1024
FROM
    pg_stat_statements;
