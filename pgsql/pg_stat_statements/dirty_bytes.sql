----------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.pg_stat_statements.dirty_bytes
-- Item name: [{#DBNAME}] Total number of shared blocks dirtied by the statement (pg_stat_statements)
--
----------------------------------------------------------------------------------------------------------

SELECT
    sum(shared_blks_dirtied + local_blks_dirtied) * 8 * 1024
FROM
    pg_stat_statements;
