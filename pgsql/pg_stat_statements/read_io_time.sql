------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.pg_stat_statements.read_io_time
-- Item name: [{#DBNAME}] Total time the statement spent reading blocks (pg_stat_statements)
--
------------------------------------------------------------------------------------------------

SELECT
    sum(blk_read_time) / float4(100)
FROM
    pg_stat_statements;
