-------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.pg_stat_statements.write_bytes
-- Item name: [{#DBNAME}] Total number of blocks written by the statement (pg_stat_statements)
--
-------------------------------------------------------------------------------------------------

SELECT
    sum(shared_blks_written + local_blks_written FROM pg_stat_statements;
