-------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Extension Statistics
-- Item key: pgsql.pg_stat_statements.other_time
-- Item name: [{#DBNAME}] Other time spent in the statement (mostly cpu) (pg_stat_statements)
--
-------------------------------------------------------------------------------------------------

SELECT
    sum(total_time - blk_read_time - blk_write_time) / float4(100)
FROM
    pg_stat_statements;
