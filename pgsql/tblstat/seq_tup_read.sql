-------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.seq_tup_read
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of live rows fetched by sequential scans
--
-------------------------------------------------------------------------------------------

SELECT
    coalesce(seq_tup_read, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
