--------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.n_tup_ins
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of rows inserted
--
--------------------------------------------------------------------

SELECT
    coalesce(n_tup_ins, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
