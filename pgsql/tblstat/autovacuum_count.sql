--------------------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.autovacuum_count
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of times this table has been vacuumed by the autovacuum daemon
--
--------------------------------------------------------------------------------------------------------------------

SELECT
    coalesce(autovacuum_count, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
