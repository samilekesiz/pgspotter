--------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.n_live_tup
-- Item name: [{#SCHNAME} / {#TBLNAME}] Estimated number of live rows
--
--------------------------------------------------------------------------

SELECT
    coalesce(n_live_tup, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
