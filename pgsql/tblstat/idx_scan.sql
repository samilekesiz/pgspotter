------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.idx_scan
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of index scans initiated on this table
--
------------------------------------------------------------------------------------------

SELECT
    coalesce(idx_scan, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
