---------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.seq_scan
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of sequential scans
--
---------------------------------------------------------------------

SELECT
    coalesce(seq_scan, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
