-------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.idx_tup_fetch
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of live rows fetched by index scans
--
-------------------------------------------------------------------------------------

SELECT
    coalesce(idx_tup_fetch, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
