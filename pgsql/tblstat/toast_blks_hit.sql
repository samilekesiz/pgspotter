-----------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.toast_blks_hit
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of buffer hits in this table's TOAST table
--
-----------------------------------------------------------------------------------------------

SELECT
    coalesce(toast_blks_hit, 0)
FROM
    pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
