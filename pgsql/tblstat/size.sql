-----------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.size
-- Item name: [{#SCHNAME} / {#TBLNAME}] Table Size
--
-----------------------------------------------------

SELECT
    pg_relation_size(:SCHTBLNAME);
