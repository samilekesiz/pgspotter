------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.analyze_count
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of times this table has been analyzed by the autovacuum daemon
--
------------------------------------------------------------------------------------------------

SELECT
    coalesce(autoanalyze_count, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
