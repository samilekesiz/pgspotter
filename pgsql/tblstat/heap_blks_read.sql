-----------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.heap_blks_read
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of disk blocks read from this table
--
-----------------------------------------------------------------------------------------

SELECT
    coalesce(heap_blks_read, 0)
FROM
    pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
