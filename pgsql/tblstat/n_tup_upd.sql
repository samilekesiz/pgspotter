-------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.n_tup_upd
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of rows updated
--
-------------------------------------------------------------------

SELECT
    coalesce(n_tup_upd, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
