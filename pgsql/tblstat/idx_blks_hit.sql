------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.idx_blks_hit
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of buffer hits in all indexes on this table
--
------------------------------------------------------------------------------------------------

SELECT
    coalesce(idx_blks_hit, 0)
FROM
    pg_statio_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
