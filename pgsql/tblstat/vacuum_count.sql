--------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Table Statistics
-- Item key: pgsql.tblstat.vacuum_count
-- Item name: [{#SCHNAME} / {#TBLNAME}] Number of times this table has been manually vacuumed
--
--------------------------------------------------------------------------------------------------

SELECT
    coalesce(vacuum_count, 0)
FROM
    pg_stat_user_tables
WHERE (schemaname || '.' || relname) = :SCHTBLNAME;
