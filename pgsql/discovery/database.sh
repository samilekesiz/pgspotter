#/usr/bin/bash

############################################
#
# Discovery name: PG Database Discovery
# Discovery key: pgsql.discovery.database
#
############################################

DBLIST=$(psql -qAtX -h "$1" -p "$2" -U "$3" -d "$4" -c "SELECT datname FROM pg_database WHERE datistemplate = false;")

echo -n '{"data":[';

for db in $DBLIST; do
  echo -n "{\"{#DBNAME}\": \"$db\"},";
done | sed -e 's:,$::';
echo -n ']}'
