#/usr/bin/bash

############################################
#
# Discovery name: PG Schema Discovery
# Discovery key: pgsql.discovery.schema
#
############################################

DBLIST=$(psql -qAtX -h "$1" -p "$2" -U "$3" -d "$4" -c "SELECT datname FROM pg_database WHERE datistemplate = false;")

echo -n '{"data":[';
for db in $DBLIST; do
  SCHEMALIST=$(psql -qAtX -h "$1" -p "$2" -U "$3" -d "$db" -c "SELECT DISTINCT schemaname FROM pg_tables WHERE schemaname NOT IN ('pg_catalog','information_schema');");
  for sch in $SCHEMALIST; do
      echo -n "{\"{#DBNAME}\": \"$db\",\"{#SCHNAME}\": \"$sch\"},";
  done
done | sed -e 's:,$::';
echo -n ']}'
