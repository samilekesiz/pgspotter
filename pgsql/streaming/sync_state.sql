-------------------------------------------------------------
--
-- Discovery rule: PG Streaming Replication
-- Application: PG Streaming Replication
-- Item key: pgsql.streaming.sync_state
-- Item name: [{#STANDBY}] Synchronous state
--
--------------------------------------------------------------

SELECT
    sync_state
FROM
    pg_stat_replication
WHERE
    client_addr = :REPNAME;
