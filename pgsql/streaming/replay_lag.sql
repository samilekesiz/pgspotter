------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Streaming Replication
-- Application: PG Streaming Replication
-- Item key: pgsql.streaming.backend_start
-- Item name: [{#STANDBY}] Flushing recent WAL and server has written, flushed and applied it
--
------------------------------------------------------------------------------------------------------

SELECT
    replay_lag
FROM
    pg_stat_replication
WHERE
    client_addr = :REPNAME;
