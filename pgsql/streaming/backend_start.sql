-------------------------------------------------------------
--
-- Discovery rule: PG Streaming Replication
-- Application: PG Streaming Replication
-- Item key: pgsql.streaming.backend_start
-- Item name: [{#STANDBY}] Streaming replication start time
--
--------------------------------------------------------------

SELECT
    backend_start
FROM
    pg_stat_replication
WHERE
    client_addr = :REPNAME;
