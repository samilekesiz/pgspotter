--------------------------------------------------------------------------------------
--
-- Discovery rule: PG Streaming Replication
-- Application: PG Streaming Replication
-- Item key: pgsql.streaming.flush_lag
-- Item name: [{#STANDBY}] Flushing recent WAL and server has written and flushed it
--
--------------------------------------------------------------------------------------

SELECT
    flush_lag
FROM
    pg_stat_replication
WHERE
    client_addr = :REPNAME;
