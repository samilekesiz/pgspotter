--------------------------------------------
--
-- Application: PG Streaming Replication
-- Item key: pgsql.streaming.count
-- Item name: Streaming replication count
--
--------------------------------------------

SELECT
    count(*)
FROM
    pg_stat_replication;
