-------------------------------------------
--
-- Application: PG Connections
-- Item key: pgsql.connections.prepared
-- Item name: Prepared connections
--
-------------------------------------------

SELECT
    count(*)
FROM
    pg_prepared_xacts;
