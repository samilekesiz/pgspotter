-----------------------------------------------------
--
-- Discovery rule: PG Table Discovery
-- Application: {#DBNAME} - Index Statistics
-- Item key: pgsql.idxstat.size
-- Item name: [{#SCHNAME} / {#TBLNAME} / {#IDXNAME}] Index Size
--
-----------------------------------------------------

SELECT
    pg_relation_size(:SCHIDXNAME);
