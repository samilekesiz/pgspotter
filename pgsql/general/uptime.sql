-----------------------------------------
--
-- Application: PG General Informations
-- Item key: pgsql.general.uptime
-- Item name: Uptime
--
-----------------------------------------

SELECT
    date_part('epoch', now() - pg_postmaster_start_time())::int;
