----------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Lock Informations
-- Item key: pgsql.lock.row_exclusive
-- Item name: [{#DBNAME}] Row Exclusive locks
--
----------------------------------------------------

SELECT
    count(*)
FROM
    pg_catalog.pg_locks
WHERE
    mode = 'RowExclusiveLock';
