----------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Lock Informations
-- Item key: pgsql.lock.access_share
-- Item name: [{#DBNAME}] Access Share locks
--
----------------------------------------------------

SELECT
    count(*)
FROM
    pg_catalog.pg_locks
WHERE
    mode = 'AccessShareLock';
