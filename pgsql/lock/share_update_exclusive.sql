-----------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Lock Informations
-- Item key: pgsql.lock.share_update_exclusive
-- Item name: [{#DBNAME}] Share Update Exclusive locks
--
-----------------------------------------------------------

SELECT
    count(*)
FROM
    pg_catalog.pg_locks
WHERE
    mode = 'ShareUpdateExclusiveLock';
