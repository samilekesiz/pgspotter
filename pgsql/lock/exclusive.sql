----------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Lock Informations
-- Item key: pgsql.lock.exclusive
-- Item name: [{#DBNAME}] Exclusive locks
--
----------------------------------------------------

SELECT
    count(*)
FROM
    pg_catalog.pg_locks
WHERE
    mode = 'ExclusiveLock';
