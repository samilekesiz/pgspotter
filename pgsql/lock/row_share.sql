----------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Lock Informations
-- Item key: pgsql.lock.row_share
-- Item name: [{#DBNAME}] Row Share locks
--
----------------------------------------------------


SELECT
    count(*)
FROM
    pg_catalog.pg_locks
WHERE
    mode = 'RowShareLock';
