-----------------------------------------------------------
--
-- Application: PG WAL Informations
-- Item key: pgsql.wal.file_count
-- Item name: Number of WAL file that ready to archive
--
-----------------------------------------------------------

SELECT
    count(name) AS count_files
FROM (
    SELECT
        name
    FROM
        pg_ls_dir('pg_wal/archive_status')
        name
    WHERE
    RIGHT (name, 6) = '.ready') ready;
