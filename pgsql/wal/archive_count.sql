---------------------------------------------------
--
-- Application: PG WAL Informations
-- Item key: pgsql.wal.archive_count
-- Item name: Number of WAL files are archived
--
---------------------------------------------------

SELECT
    archived_count
FROM
    pg_stat_archiver;
