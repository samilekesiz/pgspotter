-----------------------------------------------------------
--
-- Application: PG WAL Informations
-- Item key: pgsql.wal.failed_archive_count
-- Item name: Number of WAL files are failed to archive
--
-----------------------------------------------------------

SELECT
    failed_count
FROM
    pg_stat_archiver;
