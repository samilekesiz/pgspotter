-----------------------------------------------------------
--
-- Application: PG WAL Informations
-- Item key: pgsql.wal.write
-- Item name: Created WAL files until now
--
-----------------------------------------------------------

SELECT
    pg_catalog.pg_wal_lsn_diff (pg_catalog.pg_current_wal_lsn (),
    '0/00000000');
