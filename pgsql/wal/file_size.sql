-----------------------------------------------------------
--
-- Application: PG WAL Informations
-- Item key: pgsql.wal.file_size
-- Item name: Size of WAL files that ready to archive
--
-----------------------------------------------------------

SELECT
    coalesce(sum((pg_stat_file('pg_wal' || rtrim(ready.name, '.ready'))).size), 0) AS size_files
FROM (
    SELECT
        name
    FROM
        pg_ls_dir('pg_wal/archive_status')
        name
    WHERE
    RIGHT (name, 6) = '.ready') ready;
