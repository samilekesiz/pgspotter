---------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.oldest_transaction
-- Item name: Oldest transaction running time
--
---------------------------------------

SELECT
    CASE WHEN extract(epoch FROM max(now() - xact_start)) IS NOT NULL
        AND extract(epoch FROM max(now() - xact_start)) > 0 THEN
        extract(epoch FROM max(now() - xact_start))
    ELSE
        0
    END
FROM
    pg_catalog.pg_stat_activity
WHERE
    pid NOT IN (
        SELECT
            pid
        FROM
            pg_stat_replication)
    AND pid <> pg_backend_pid();
