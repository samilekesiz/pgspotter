---------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.oldest_xid
-- Item name: Oldest XID age
--
---------------------------------------

SELECT
    greatest (max(age(backend_xmin)),
        max(age(backend_xid)))
FROM
    pg_catalog.pg_stat_activity;
