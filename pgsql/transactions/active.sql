---------------------------------------
--
-- Application: PG Transactions
-- Item key: pgsql.transactions.active
-- Item name: Active transactions
--
---------------------------------------

SELECT
    coalesce(extract(epoch FROM max(age(now(), query_start))), 0)
FROM
    pg_stat_activity
WHERE
    state <> 'idle in transaction'
    AND state <> 'idle';
