#!/usr/bin/bash

##############################################################
#
# Application name: PG Other Informations
# Item name: Number of pending updates from PGDG repository
# Item key: pgsql.other.pgdg_updates
#
##############################################################

yum check-updates | grep pgdg | awk '{print $1;}' | wc -l
