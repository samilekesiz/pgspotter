--------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_deleted
-- Item name: [{#DBNAME}] Number of rows deleted by queries in this database
--
--------------------------------------------------------------------------------

SELECT
    tup_deleted
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
