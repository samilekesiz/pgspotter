------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat_numbackends
-- Item name: [{#DBNAME}] Number of backends currently connected to this database
--
------------------------------------------------------------------------------------------------

SELECT
    numbackends
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
