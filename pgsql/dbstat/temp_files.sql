--------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.temp_files
-- Item name: [{#DBNAME}] Number of temporary files created by queries in this database
--
--------------------------------------------------------------------------------------------

SELECT
    temp_files
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
