----------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_inserted
-- Item name: [{#DBNAME}] Number of rows inserted by queries in this database
--
----------------------------------------------------------------------------------

SELECT
    tup_inserted
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
