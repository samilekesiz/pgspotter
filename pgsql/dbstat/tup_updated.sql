---------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_updated
-- Item name: [{#DBNAME}] Number of rows updated by queries in this database
--
---------------------------------------------------------------------------------

SELECT
    tup_updated
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
