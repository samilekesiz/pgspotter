-------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: PG Database Statistics
-- Item key: pgsql.dbstat.blks_read
-- Item name: [{#DBNAME}] Number of disk blocks read in this database
--
-------------------------------------------------------------------------

SELECT
    blks_read
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
