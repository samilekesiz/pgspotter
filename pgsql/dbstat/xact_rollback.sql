-------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.xact_rollback
-- Item name: [{#DBNAME}] Number of transactions in this database that have been rolled back
--
-------------------------------------------------------------------------------------------------

SELECT
    xact_rollback
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
