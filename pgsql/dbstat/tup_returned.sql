---------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_returned
-- Item name: [{#DBNAME}] Number of rows returned by queries in this database
--
---------------------------------------------------------------------------------

SELECT
    tup_returned
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
