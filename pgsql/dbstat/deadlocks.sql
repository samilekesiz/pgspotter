---------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.deadlocks
-- Item name: [{#DBNAME}] Number of deadlocks detected in this database
--
---------------------------------------------------------------------------


SELECT
    deadlocks
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
