--------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.tup_fetched
-- Item name: [{#DBNAME}] Number of rows fetched by queries in this database
--
--------------------------------------------------------------------------------

SELECT
    tup_fetched
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
