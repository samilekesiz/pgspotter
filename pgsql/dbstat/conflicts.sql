---------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.conflicts
-- Item name: [{#DBNAME}] Number of queries canceled due to conflicts with recovery in this database
--
---------------------------------------------------------------------------------------------------------

SELECT
    conflicts
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
