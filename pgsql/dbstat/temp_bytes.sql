--------------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.temp_bytes
-- Item name: [{#DBNAME}] Total amount of data written to temporary files by queries in this database
--
--------------------------------------------------------------------------------------------------------

SELECT
    temp_bytes
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
