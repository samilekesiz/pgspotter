------------------------------------------------------------------------------------------------
--
-- Discovery rule: PG Database Discovery
-- Application: {#DBNAME} - Database Statistics
-- Item key: pgsql.dbstat.blks_hit
-- Item name: [{#DBNAME}] Number of times disk blocks were found already in the buffer cache
--
------------------------------------------------------------------------------------------------

SELECT
    blks_hit
FROM
    pg_stat_database
WHERE
    datname = :DATABNAME;
